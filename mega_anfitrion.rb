class MegaAnfitrion
  attr_accessor :nombres

  def initialize(_nombre='Mundo')
    @nombres
  end

  def decir_hola
    if @nombres.nil?
      puts '...'
    elsif @nombres.respond_to?('each')
      @nombres.each do |nombre|
        puts "Hola #{nombre}"
      end
    else
      puts "Hola #{@nombres}"
    end
  end
end

if $PROGRAM_NAME == __FILE__
  ma = MegaAnfitrion.new
  ma.decir_hola
end
